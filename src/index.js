import ReactDOM from 'react-dom'
import routes from './routes'
import './style/index.css'

ReactDOM.render(
  routes,
  document.getElementById('root')
)
