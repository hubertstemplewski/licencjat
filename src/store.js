import { createStore } from 'redux'
import { syncHistoryWithStore } from 'react-router-redux'
import { browserHistory } from 'react-router'

import rootReducer from './reducers/index'

// import data
import menus from './data/menus'
import dishes from './data/dishes'
import products from './data/products'

const initialState = {
  menus,
  dishes,
  products
}

const store = createStore(rootReducer, initialState)

export const history = syncHistoryWithStore(browserHistory, store)

export default store
