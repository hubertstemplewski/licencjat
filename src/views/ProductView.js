import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as actionCreators from '../actions/actionsCreators'
import PropTypes from 'prop-types'
import TextField from 'material-ui/TextField'
import { Toolbar, ToolbarTitle, ToolbarGroup } from 'material-ui/Toolbar'
import Paper from 'material-ui/Paper'
import FlatButton from 'material-ui/FlatButton'
import ProductItemList from '../components/ProductItemList'
import AddEditProductModal from '../components/AddEditProductModal'

const mapStateToProps = ({ products }) => ({ products })
const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(actionCreators, dispatch)
})

class ProductView extends Component {
  constructor (props) {
    super(props)
    this.state = {
      products: this.props.products,
      openAddEditModal: false,
      editedProduct: null
    }
  }
  componentWillUpdate (nextProps) {
    if (nextProps.products !== this.props.products) {
      this.setState({ products: nextProps.products })
    }
  }
  onFilter = (e) => {
    const filteredProducts = this.props.products.filter((product) => {
      if (product.name.toLowerCase().match(e.currentTarget.value)) return product
    })
    this.setState({ products: filteredProducts })
  }
  openEditModal = (product) => {
    this.setState({
      editedProduct: product,
      openAddEditModal: true
    })
  }
  openCloseAddModal = () => {
    const { openAddEditModal } = this.state
    this.setState({
      openAddEditModal: !openAddEditModal,
      editedProduct: null
    })
  }
  onAddProduct = (product) => {
    this.props.actions.addProduct(product)
    this.setState({ openAddEditModal: false })
  }
  onEditProduct = (product) => {
    this.props.actions.editProduct(product)
    this.setState({ openAddEditModal: false, editedProduct: null })
  }
  render () {
    return (
      <Paper zDepth={2} className='content-box-history'>
        <Toolbar className='content-box-header flex-center'>
          <ToolbarTitle text='Biblioteka Produktów' />
          <ToolbarGroup>
            <TextField
              hintText='Filtruj po nazwach'
              hintStyle={{ color: '#fff' }}
              inputStyle={{ color: '#fff' }}
              onChange={(e) => this.onFilter(e)} />
            <FlatButton
              secondary
              label='Dodaj Produkt'
              onClick={() => this.openCloseAddModal()}
            />
          </ToolbarGroup>
        </Toolbar>
        <ProductItemList
          openEditModal={this.openEditModal}
          products={this.state.products}
        />
        <AddEditProductModal
          editedProduct={this.state.editedProduct}
          closeModal={this.openCloseAddModal}
          onAddProduct={this.onAddProduct}
          onEditProduct={this.onEditProduct}
          open={this.state.openAddEditModal}
        />
      </Paper>
    )
  }
}

ProductView.propTypes = {
  products: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductView)
