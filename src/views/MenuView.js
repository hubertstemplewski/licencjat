import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actionCreators from '../actions/actionsCreators'
import { Toolbar, ToolbarTitle } from 'material-ui/Toolbar'
import Paper from 'material-ui/Paper'
import FlatButton from 'material-ui/FlatButton'
import DishItem from '../components/DishItem'

function mapStateToProps (state) {
  return {
    dishes: state.dishes,
    menus: state.menus,
    products: state.products
  }
}

function mapDispatchToProps (dispatch) {
  return { action: bindActionCreators(actionCreators, dispatch) }
}

class MenuView extends Component {
  handleRemove = (currentMenu) => {
    this.props.router.goBack()
    this.props.action.removeMenu(currentMenu)
  }

  render () {
    const currentMenuID = this.props.router.location.pathname.match(/catering/)
    ? this.props.router.location.pathname.slice(10)
    : this.props.router.location.pathname.slice(12)
    console.log(currentMenuID)
    const currentMenu = this.props.menus.find((el) => {
      if (el.id === currentMenuID) return el
    })
    const dishesToRender = currentMenu.dishes.map((dishID) => {
      return this.props.dishes.filter((dish) => {
        if (dish.id === dishID) return dish
      })
    })

    console.log(this.props.products)
    const dishItems = dishesToRender.length
    ? dishesToRender.map((dish, i) => {
      return (
        <DishItem
          dish={dish[0]}
          products={this.props.products}
          key={i} />
      )
    })
    : <Paper zDepth={1} className='flex-center' style={{ padding: 20 }}>
      <h1>Nie ma dań w tym menu</h1>
    </Paper>
    return (
      <Paper zDepth={2} className='current-menu-view'>
        <Toolbar className='current-menu-view-header flex-center'>
          <ToolbarTitle text={currentMenu.name} />
          <FlatButton label='Usuń to menu' secondary onClick={() => { this.handleRemove(currentMenu) }} />
        </Toolbar>
        <div className='current-menu-view-fixed-header flex-center'>
          {dishItems}
        </div>
      </Paper>
    )
  }
}

MenuView.propTypes = {
  router: PropTypes.object.isRequired,
  action: PropTypes.object.isRequired,
  menus: PropTypes.array.isRequired,
  dishes: PropTypes.array.isRequired,
  products: PropTypes.array.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(MenuView)
