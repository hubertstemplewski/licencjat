import React, { Component } from 'react'
import { Link } from 'react-router'
import { Card } from 'material-ui/Card'

const menuOptions = [
  {
    id: 1,
    name: 'Restauracja',
    path: 'restaurant/library',
    options: ['Biblioteka', 'Menu', 'Produkty']
  },
  {
    id: 2,
    name: 'Catering',
    path: 'catering/library',
    options: ['Biblioteka', 'Menu', 'Produkty']
  },
  {
    id: 3,
    name: 'Historia',
    path: 'history',
    options: ['Restauracji', 'Imprez Cateringowych']
  }
]

const DashboardElement = menuOptions.map((el, i) => {
  return (
    <Card
      key={i}
      className='dashboard-element'>
      <Link to={`/${el.path}`}>
        <div key={i} >{el.name}</div>
      </Link>
    </Card>
  )
})

export default class DashboardView extends Component {
  render () {
    return (
      <div className='flex-center dashboard'>
        { DashboardElement.map((el) => el) }
      </div>
    )
  }
}
