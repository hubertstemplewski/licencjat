import React, { Component } from 'react'
import Paper from 'material-ui/Paper'
import AppBarOptions from '../components/AppBarOptions'
import LogoPlaceholder from '../graphics/logo-placeholder.png'
import { Link } from 'react-router'
import {
  AppBar
 } from 'material-ui'

const centerStyle = {
  display: 'flex',
  alignItems: 'center'
}

const logoStyle = {
  maxHeight: 50
}

const logo = <Link to='/' style={{ height: 50 }}><img src={LogoPlaceholder} style={logoStyle} alt='Logo' /></Link>

class AppBodyView extends Component {
  render () {
    return (
      <div>
        <AppBar
          showMenuIconButton={false}
          title={logo}
          titleStyle={centerStyle}
          style={centerStyle}
          children={<AppBarOptions />} />
        <div className='content-styles'>
          {this.props.children}
        </div>
        <Paper zDepth={3} className='footer-styles'>Hubert Stemplewski ©</Paper>
      </div>
    )
  }
}

export default AppBodyView
