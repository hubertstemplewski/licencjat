import React, { Component } from 'react'
// import ReactDOM from 'react-dom'
import FusionCharts from 'fusioncharts'
// Load the charts module
import charts from 'fusioncharts/fusioncharts.charts'
import ReactFC from 'react-fusioncharts'
// material-ui
import { Toolbar, ToolbarTitle } from 'material-ui/Toolbar'
import Paper from 'material-ui/Paper'

// Pass fusioncharts as a dependency of charts
charts(FusionCharts)

var myDataSource = {
  chart: {
    'caption': 'Statystyki sprzedaży',
    'subcaption': 'Ostatni rok',
    'xaxisname': 'Miesiąc',
    'yaxisname': 'Wartość (Zł)',
    'theme': 'ocean'
  },
  'categories': [
    {
      'category': [
        {
          'label': 'Sty'
        },
        {
          'label': 'Lut'
        },
        {
          'label': 'Mar'
        },
        {
          'label': 'Kwi'
        },
        {
          'label': 'Maj'
        },
        {
          'label': 'Cze'
        },
        {
          'label': 'Lip'
        },
        {
          'label': 'Sie'
        },
        {
          'label': 'Wrz'
        },
        {
          'label': 'Paź'
        },
        {
          'label': 'Lis'
        },
        {
          'label': 'Gru'
        }
      ]
    }
  ],
  'dataset': [
    {
      'seriesname': 'Utarg',
      'data': [
        {
          'value': '20000'
        },
        {
          'value': '25000'
        },
        {
          'value': '18000'
        },
        {
          'value': '25000'
        },
        {
          'value': '18000'
        },
        {
          'value': '34000'
        },
        {
          'value': '27000'
        },
        {
          'value': '35000'
        },
        {
          'value': '18000'
        },
        {
          'value': '13000'
        },
        {
          'value': '15000'
        },
        {
          'value': '10000'
        }
      ]
    },
    {
      'seriesname': 'Prognozowany utarg',
      'renderas': 'line',
      'showvalues': '0',
      'data': [
        {
          'value': '24000'
        },
        {
          'value': '26000'
        },
        {
          'value': '16000'
        },
        {
          'value': '12000'
        },
        {
          'value': '16000'
        },
        {
          'value': '22000'
        },
        {
          'value': '25000'
        },
        {
          'value': '29000'
        },
        {
          'value': '17000'
        },
        {
          'value': '15000'
        },
        {
          'value': '14000'
        },
        {
          'value': '13000'
        }
      ]
    },
    {
      'seriesname': 'Zysk',
      'renderas': 'area',
      'showvalues': '0',
      'data': [
        {
          'value': '10000'
        },
        {
          'value': '11000'
        },
        {
          'value': '8000'
        },
        {
          'value': '9000'
        },
        {
          'value': '8000'
        },
        {
          'value': '17000'
        },
        {
          'value': '14000'
        },
        {
          'value': '15000'
        },
        {
          'value': '10000'
        },
        {
          'value': '8000'
        },
        {
          'value': '7000'
        },
        {
          'value': '5000'
        }
      ]
    }]
}
var chartConfigs = {
  id: 'revenue-profits-chart',
  renderAt: 'revenue-profits-chart-container',
  type: 'mscombi2d',
  width: '100%',
  height: '60%',
  dataFormat: 'json',
  dataSource: myDataSource
}

class HistoryView extends Component {
  render () {
    return (
      <Paper zDepth={2} className='content-box-history'>
        <Toolbar className='content-box-header flex-center'>
          <ToolbarTitle text='Historia' />
        </Toolbar>
        <div style={{ width: '90%', margin: '3rem auto' }}>
          <ReactFC {...chartConfigs} />
        </div>
      </Paper>
    )
  }
}

export default HistoryView
