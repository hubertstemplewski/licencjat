import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import * as actionCreators from '../actions/actionsCreators'
import { connect } from 'react-redux'
import SideNav from '../components/SideNav'
import MainLibrary from '../components/MainLibrary'

function mapStateToProps (state) {
  return {
    menus: state.menus,
    products: state.products
  }
}

function mapDispatchToProps (dispatch) {
  return { action: bindActionCreators(actionCreators, dispatch) }
}

class RestaurantView extends Component {
  render () {
    const menus = this.props.menus.filter((menu) => {
      return !menu.inCatering && menu
    })
    return (
      <div>
        <SideNav
          title='Menu'
          menus={menus} />
        <MainLibrary
          title='Biblioteka Dań'
          products={this.props.products} />
      </div>
    )
  }
}

RestaurantView.propTypes = {
  menus: PropTypes.array.isRequired,
  products: PropTypes.array.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(RestaurantView)
