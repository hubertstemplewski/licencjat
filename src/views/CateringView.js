import React, { Component } from 'react'
import { bindActionsCreactors } from 'redux'
import { connect } from 'react-redux'
import SideNav from '../components/SideNav'
import MainLibrary from '../components/MainLibrary'

function mapStateToProps(state){
  return {
    menus: state.menus,
    dishes: state.dishes,
    products: state.products
  }
}

class CateringView extends Component {
  render(){
    const cateringMenus = this.props.menus.filter((menu) => {
      return menu.inCatering && menu
    })
    return (
      <div>
        <SideNav
          title='Catering'
          menus={cateringMenus} />
        <MainLibrary
          title='Biblioteka Dań'
          dishes={this.props.dishes}
          products={this.props.products} />
      </div>
    )
  }
}

export default connect(mapStateToProps)(CateringView)
