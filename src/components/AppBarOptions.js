import React, { Component } from 'react'
import IconButton from 'material-ui/IconButton'
import { Link } from 'react-router'

class AppBarOptions extends Component {
  render () {
    return (
      <div>
        <Link to='/catering/library'>
          <IconButton
            iconClassName='material-icons'
            tooltip='Catering' >
            directions_car
          </IconButton>
        </Link>
        <Link to='/restaurant/library'>
          <IconButton
            iconClassName='material-icons'
            tooltip='Restauracja' >
            restaurant_menu
          </IconButton>
        </Link>
        <Link to='/products'>
          <IconButton
            iconClassName='material-icons'
            tooltip='Produkty' >
            shopping_cart
          </IconButton>
        </Link>
        <Link to='/history'>
          <IconButton
            iconClassName='material-icons'
            tooltip='Historia' >
            assessment
          </IconButton>
        </Link>
      </div>
    )
  }
}

export default AppBarOptions
