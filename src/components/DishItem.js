import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Card, CardTitle, CardText } from 'material-ui/Card'
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'

class DishItem extends Component {
  render () {
    const { dish } = this.props
    const productsList = dish ? dish.products.map((product, i) => {
      return this.props.products.find((prod) => {
        if (prod.id === product.id) return prod
      })
    }) : []
    return (
      <Card className='dish-item clearfix'>
        <img className='dish-item-img' src={dish.picUrl ? dish.picUrl : 'http://placehold.it/160x120'} alt='dish' />
        <CardTitle title={dish.name} subtitle={`Cena: ${dish.dishPrice} zł`} className='dish-item-title' />
        <p>Lista produktów</p>
        <ul>
          {productsList.length ? productsList.map((el, i) => {
            return <li key={i}>{el.name} - {el.price} zł / {el.priceForKg ? 'kg' : 'szt'}</li>
          }) : <li>Nie ma produktów na liście</li> }
        </ul>
        <CardText>
          {dish.description}
        </CardText>
        {
          window.location.pathname.match(/library/)
          ? <div>
            <FlatButton
              className='btn-styles'
              secondary
              label='Usuń danie'
              onClick={() => this.props.removeDish(dish)} />
            <RaisedButton
              className='btn-styles'
              primary
              label='Edytuj danie'
              onClick={() => { this.props.onEditDish(dish) }} />
          </div>
          : <span />
        }
      </Card>
    )
  }
}

DishItem.propTypes = {
  dish: PropTypes.object.isRequired,
  products: PropTypes.array.isRequired,
  removeDish: PropTypes.func,
  onEditDish: PropTypes.func
}

export default DishItem
