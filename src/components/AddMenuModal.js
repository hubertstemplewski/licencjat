import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'
import TextField from 'material-ui/TextField'
import Checkbox from 'material-ui/Checkbox'
import ActionFavorite from 'material-ui/svg-icons/action/favorite'
import ActionFavoriteBorder from 'material-ui/svg-icons/action/favorite-border'
import Paper from 'material-ui/Paper'
import * as Helpers from '../Helpers/generateID'
import { connect } from 'react-redux'

function mapStateToProps (state) {
  return {
    dishes: state.dishes
  }
}

class AddMenuModal extends Component {
  constructor (props) {
    super(props)
    this.state = {
      menuObj: {}
    }
  }
  componentWillReceiveProps (nextProps) {
    if (nextProps.editedMenu) {
      this.setState({ menuObj: nextProps.editedMenu })
    }
    if (this.props.editedMenu && !nextProps.editedMenu) {
      this.setState({ menuObj: {} })
    }
  }

  onChangeName = (e) => {
    const menuObj = this.state.menuObj
    const newMenuObj = { ...menuObj }
    newMenuObj.id = menuObj.id
    ? menuObj.id
    : Helpers.generateID()
    newMenuObj.name = e.currentTarget.value
    newMenuObj.inCatering = !!window.location.pathname.match(/catering/)
    newMenuObj.dishes = menuObj.dishes
    ? menuObj.dishes
    : []
    this.setState({ menuObj: newMenuObj })
  }

  onChooseDishes = (e) => {
    const dishID = e.currentTarget.value
    const { menuObj } = this.state
    console.log(menuObj)
    let newMenuObj = Object.assign({}, menuObj)
    const indexOfDish = newMenuObj.dishes.indexOf(dishID)
    console.log(indexOfDish)
    if (indexOfDish === -1) {
      newMenuObj.dishes.push(dishID)
    } else {
      newMenuObj.dishes = [
        ...newMenuObj.dishes.slice(0, indexOfDish),
        ...newMenuObj.dishes.slice(indexOfDish + 1)
      ]
    }
    this.setState({ menuObj: newMenuObj })
  }
  defaultDishes = (dishID) => {
    const defaultCheck = this.state.menuObj.dishes
    ? this.state.menuObj.dishes.find((dishId) => {
      if (dishId === dishID) return true
    }) : null
    return defaultCheck
  }

  render () {
    const actions = <div>
      <FlatButton secondary label='Anuluj' onClick={this.props.onClose} />
      <RaisedButton
        primary label={this.props.editedMenu ? 'Zapisz' : 'Dodaj'}
        onClick={() => {
          this.props.onAddMenu(this.state.menuObj)
          this.setState({ menuObj: {} })
        }} />
    </div>
    const dishes = this.props.dishes.map((dish) => {
      return (
        <Checkbox
          key={dish.id}
          value={dish.id}
          disabled={!this.state.menuObj.name}
          defaultChecked={!!this.defaultDishes(dish.id)}
          checkedIcon={<ActionFavorite />}
          uncheckedIcon={<ActionFavoriteBorder />}
          onCheck={(e) => this.onChooseDishes(e)}
          label={dish.name}
        />
      )
    })
    return (
      <Dialog
        title={this.props.editedMenu ? 'Edytuj Menu' : 'Dodaj Nowe Menu'}
        open={this.props.open}
        actions={actions}
        onRequestClose={this.props.onClose}>
        <TextField
          fullWidth
          floatingLabelText='Nazwa Menu'
          defaultValue={this.state.menuObj.name ? this.state.menuObj.name : ''}
          hintText='Nazwa Menu'
          onChange={(e) => this.onChangeName(e)} />
        <Paper zDepth={1} style={{ marginTop: 15 }}>
          <div style={{ padding: 15 }}>
            <h4 style={{ marginTop: 0 }}>Wybierz Dania Dla Tego Menu:</h4>
            {dishes}
          </div>
        </Paper>
      </Dialog>
    )
  }
}

AddMenuModal.propTypes = {
  editedMenu: PropTypes.object,
  onClose: PropTypes.func.isRequired,
  onAddMenu: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  dishes: PropTypes.array.isRequired
}

export default connect(mapStateToProps)(AddMenuModal)
