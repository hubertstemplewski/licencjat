import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Drawer from 'material-ui/Drawer'
import MenuItem from 'material-ui/MenuItem'
import { Toolbar, ToolbarTitle } from 'material-ui/Toolbar'
import IconMenu from 'material-ui/IconMenu'
import IconButton from 'material-ui/IconButton'
import ActionSettings from 'material-ui/svg-icons/action/settings'
import ActionDelete from 'material-ui/svg-icons/action/delete'
import { Link } from 'react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as actionCreators from '../actions/actionsCreators'
import AddMenuModal from './AddMenuModal'
import ListMenusToEdit from './ListMenusToEdit'

function mapDispatchToProps (dispatch) {
  return { actions: bindActionCreators(actionCreators, dispatch) }
}

class SideNav extends Component {
  constructor (props) {
    super(props)
    this.state = {
      editedMenu: null,
      openAdd: false,
      openEdit: false
    }
  }

  openEditModal = (editedMenu) => {
    this.setState({ openAdd: true, editedMenu })
  }

  onCloseEditMenuModal = () => {
    this.setState({ openEdit: false, editedMenu: null })
  }

  onCloseAddMenuModal = () => {
    this.setState({ openAdd: false })
  }

  onAddMenu = (menuObj) => {
    if (this.state.editedMenu) {
      this.props.actions.editMenu(menuObj)
    } else {
      this.props.actions.addMenu(menuObj)
    }
    console.log('wykonuje')
    this.setState({ openAdd: false, editedMenu: null })
  }

  render () {
    return (
      <Drawer open containerClassName='sidenav'>
        <Toolbar className='sidenav-header flex-center'>
          <ToolbarTitle text={this.props.title} />
          <IconMenu
            iconButtonElement={
              <IconButton touch>
                <ActionSettings />
              </IconButton>
            }>
            <MenuItem
              primaryText='Dodaj menu'
              onClick={() => this.setState({ openAdd: true })} />
            <MenuItem primaryText='Edytuj menu' onClick={() => this.setState({ openEdit: true })} />
          </IconMenu>
        </Toolbar>
        {this.props.menus.map((menu, i) => {
          return (
            <Link to={menu.inCatering ? `/catering/${menu.id}` : `/restaurant/${menu.id}`} key={i}>
              <MenuItem
                rightIconButton={<IconButton onClick={(e) => {
                  e.preventDefault()
                  this.props.actions.removeMenu(menu)
                }}>
                  <ActionDelete color='#ff0000' />
                </IconButton>}>{menu.name}</MenuItem>
            </Link>
          )
        })}
        <ListMenusToEdit
          open={this.state.openEdit}
          openAdd={this.state.openAdd}
          menus={this.props.menus}
          openEdit={this.openEditModal}
          onClose={this.onCloseEditMenuModal}
          onCloseAddMenuModal={this.onCloseAddMenuModal}
         />
        <AddMenuModal
          editedMenu={this.state.editedMenu}
          open={this.state.openAdd}
          onAddMenu={this.onAddMenu}
          onClose={this.onCloseAddMenuModal} />
      </Drawer>
    )
  }
}

SideNav.propTypes = {
  title: PropTypes.string,
  actions: PropTypes.object.isRequired,
  menus: PropTypes.array.isRequired
}

export default connect(null, mapDispatchToProps)(SideNav)
