import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Toolbar, ToolbarTitle } from 'material-ui/Toolbar'
import Paper from 'material-ui/Paper'
import DishItem from './DishItem'
import FlatButton from 'material-ui/FlatButton'
import AddDishModal from './AddDishModal'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actionCreators from '../actions/actionsCreators'

function mapStateToProps (state) {
  return {
    dishes: state.dishes
  }
}

function mapDispatchToProps (dispatch) {
  return { actions: bindActionCreators(actionCreators, dispatch) }
}

class MainLibrary extends Component {
  constructor (props) {
    super(props)
    this.state = {
      open: false,
      editedDish: null
    }
  }
  onCloseAddDishModal = () => {
    this.setState({ open: false, editedDish: null })
  }
  onAddDish = (dishObj) => {
    this.props.actions.addDish(dishObj)
    this.setState({ open: false })
  }
  onRemoveDish = (dishObj) => {
    this.props.actions.removeDish(dishObj)
  }
  onOpenEditDish = (editedDish) => {
    console.log(editedDish)
    this.setState({ editedDish, open: true })
  }
  onEditDish = (dishObj) => {
    this.props.actions.editDish(dishObj)
    this.setState({ open: false, editedDish: null })
  }
  render () {
    return (
      <Paper zDepth={2} className='content-box'>
        <Toolbar className='content-box-header flex-center'>
          <ToolbarTitle text={this.props.title} />
          <FlatButton secondary label='Dodaj Danie' onClick={() => this.setState({ open: true })} />
        </Toolbar>
        <div className='content-box-fixed-header'>
          {this.props.dishes
          ? this.props.dishes.map((dish, i) => {
            return (
              <DishItem
                dish={dish}
                products={this.props.products}
                key={i}
                removeDish={this.onRemoveDish}
                onEditDish={this.onOpenEditDish} />
            )
          })
          : <Paper zDepth={1} className='flex-center' style={{ padding: 20 }}>
            <h1>Nie ma dań w bibliotece</h1>
          </Paper> }
        </div>
        <AddDishModal
          open={this.state.open}
          editedDish={this.state.editedDish}
          onAddDish={this.onAddDish}
          onEditDish={this.onEditDish}
          onClose={this.onCloseAddDishModal} />
      </Paper>
    )
  }
}

MainLibrary.propTypes = {
  title: PropTypes.string,
  actions: PropTypes.object.isRequired,
  dishes: PropTypes.array.isRequired,
  products: PropTypes.array.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(MainLibrary)
