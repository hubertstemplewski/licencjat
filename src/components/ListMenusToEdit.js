import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import { List, ListItem } from 'material-ui/List'

class ListMenusToEdit extends Component {
  render () {
    const actions = <div>
      <FlatButton secondary label='Wróć' onClick={this.props.onClose} />
    </div>

    const menuList = this.props.menus.map((menu) => {
      return (
        <ListItem
          key={menu.id}
          primaryText={menu.name}
          onClick={() => this.props.openEdit(menu)}
          secondaryText='Kliknij aby edytować'
        />
      )
    })

    return (
      <Dialog
        title='Wybierz Menu Do Edycji'
        open={this.props.open}
        actions={actions}
        onRequestClose={this.props.onClose}>
        <List>
          {menuList}
        </List>
      </Dialog>
    )
  }
}

ListMenusToEdit.propTypes = {
  onClose: PropTypes.func.isRequired,
  menus: PropTypes.array.isRequired,
  open: PropTypes.bool.isRequired,
  openEdit: PropTypes.func.isRequired
}

export default ListMenusToEdit
