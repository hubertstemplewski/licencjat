import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as actionCreators from '../actions/actionsCreators'
import Divider from 'material-ui/Divider'
import ActionDelete from 'material-ui/svg-icons/action/delete'
import IconButton from 'material-ui/IconButton'
import { List, ListItem } from 'material-ui/List'

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(actionCreators, dispatch)
})

class ProductItemList extends Component {
  render () {
    return (
      <List style={{ margin: '0 1rem' }}>
        {this.props.products.map((product) => {
          return (
            <div key={product.id}>
              <ListItem
                primaryText={product.name}
                secondaryText={`${product.price} zł / ${product.priceForKg ? 'kg' : 'szt'}`}
                onClick={() => this.props.openEditModal(product)}
                rightIconButton={
                  <IconButton onClick={(e) => {
                    this.props.actions.removeProduct(product)
                  }}>
                    <ActionDelete color='#ff0000' />
                  </IconButton>
                }
              />
              <Divider />
            </div>
          )
        })}
      </List>
    )
  }
}

ProductItemList.propTypes = {
  products: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired,
  openEditModal: PropTypes.func.isRequired
}

export default connect(null, mapDispatchToProps)(ProductItemList)
