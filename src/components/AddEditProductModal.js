import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'
import SelectField from 'material-ui/SelectField'
import MenuItem from 'material-ui/MenuItem'
import TextField from 'material-ui/TextField'
import * as Helpers from '../Helpers/generateID'

export default class AddEditProductModal extends Component {
  constructor (props) {
    super(props)
    this.state = {
      product: {}
    }
  }
  componentWillReceiveProps (nextProps) {
    if (nextProps.editedProduct) {
      this.setState({ product: nextProps.editedProduct })
    }
    if (this.props.editedProduct && !nextProps.editedProduct) {
      this.setState({ product: {} })
    }
  }
  onClose = () => {
    this.setState({ product: {} })
    this.props.closeModal()
  }
  onChangeName = (e) => {
    const product = this.state.product
    const newProduct = Object.assign(product, {})
    newProduct.id = product.id
    ? product.id
    : Helpers.generateID()
    newProduct.name = e.currentTarget.value
    newProduct.products = product.products
    ? product.products
    : []
    this.setState({ product: newProduct })
  }
  onChangePrice = (e) => {
    if (e.currentTarget.value.match(/(\d{1,2}\.(?=\d{1,2}))/)) {
      const { product } = this.state
      const newProduct = Object.assign({}, product)
      newProduct.price = e.currentTarget.value
      this.setState({ product: newProduct, errorPrice: '' })
    } else {
      this.setState({ errorPrice: 'Zły format ceny (np. 2.50, 2.00)' })
    }
  }
  onChangePriceFor = (event, index, value) => {
    const { product } = this.state
    const newProduct = Object.assign({}, product)
    newProduct.priceForKg = value
    this.setState({ product: newProduct })
  }
  onSubmit = () => {
    if (this.props.editedProduct) {
      this.props.onEditProduct(this.state.product)
      this.setState({ product: {} })
    } else {
      this.props.onAddProduct(this.state.product)
      this.setState({ product: {} })
    }
  }
  render () {
    const { editedProduct } = this.props
    const { product } = this.state
    const actions = <div>
      <FlatButton secondary label='Anuluj' onClick={() => this.onClose()} />
      <RaisedButton primary label={editedProduct ? 'Zapisz' : 'Dodaj'} onClick={() => this.onSubmit()} />
    </div>
    return (
      <Dialog
        actions={actions}
        title={editedProduct ? 'Edytuj Produkt' : 'Dodaj Produkt'}
        onRequestClose={this.onClose}
        open={this.props.open}>
        <TextField
          defaultValue={product ? product.name : ''}
          fullWidth
          hintText='Nazwa Produktu'
          floatingLabelText='Nazwa Produktu'
          onChange={(e) => this.onChangeName(e)}
        />
        <div className='flex-center' style={{ justifyContent: 'space-between' }}>
          <TextField
            disabled={!product.name}
            defaultValue={product ? product.price : ''}
            floatingLabelText='Cena (w zł)'
            hintText='Cena (w zł)'
            errorText={this.state.errorPrice}
            onChange={(e) => this.onChangePrice(e)} />
          <SelectField
            disabled={!product.name}
            value={product ? product.priceForKg : ''}
            onChange={this.onChangePriceFor}
            floatingLabelText='Cena za '
          >
            <MenuItem value primaryText='Kg' />
            <MenuItem value={false} primaryText='Sztukę' />
          </SelectField>
        </div>
      </Dialog>
    )
  }
}

AddEditProductModal.propTypes = {
  open: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired,
  editedProduct: PropTypes.object,
  onEditProduct: PropTypes.func.isRequired,
  onAddProduct: PropTypes.func.isRequired
}
