import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'
import TextField from 'material-ui/TextField'
import Checkbox from 'material-ui/Checkbox'
import ActionFavorite from 'material-ui/svg-icons/action/favorite'
import ActionFavoriteBorder from 'material-ui/svg-icons/action/favorite-border'
import Paper from 'material-ui/Paper'
import * as Helpers from '../Helpers/generateID'
import { connect } from 'react-redux'

function mapStateToProps (state) {
  return {
    products: state.products
  }
}

const urlRegex = /(http)?s?:?(\/\/[^"']*\.(?:png|jpg|jpeg|gif|png|svg))/

class AddDishModal extends Component {
  constructor (props) {
    super(props)
    this.state = {
      dishObj: {}
    }
  }
  componentWillReceiveProps (nextProps) {
    if (nextProps.editedDish) {
      this.setState({ dishObj: nextProps.editedDish })
    }
    if (this.props.editedDish && !nextProps.editedDish) {
      this.setState({ dishObj: {} })
    }
  }
  onClose = () => {
    this.setState({ dishObj: {} })
    this.props.onClose()
  }
  onChangeName = (e) => {
    const dishObj = this.state.dishObj
    const newDishObj = Object.assign(dishObj, {})
    newDishObj.id = dishObj.id
    ? dishObj.id
    : Helpers.generateID()
    newDishObj.name = e.currentTarget.value
    newDishObj.products = dishObj.products
    ? dishObj.products
    : []
    this.setState({ dishObj: newDishObj })
  }
  onChangeDescription = (e) => {
    const { dishObj } = this.state
    const newDishObj = Object.assign({}, dishObj)
    newDishObj['description'] = e.currentTarget.value
    this.setState({ dishObj: newDishObj })
  }
  onChangeURL = (e) => {
    if (e.currentTarget.value.match(urlRegex)) {
      const { dishObj } = this.state
      const newDishObj = Object.assign({}, dishObj)
      newDishObj['picUrl'] = e.currentTarget.value
      this.setState({ dishObj: newDishObj, error: '' })
    } else {
      this.setState({ error: 'Niepoprawny adres URL' })
    }
  }
  onChangePrice = (e) => {
    if (e.currentTarget.value.match(/^\d+$/)) {
      const { dishObj } = this.state
      const newDishObj = Object.assign({}, dishObj)
      newDishObj['dishPrice'] = e.currentTarget.value
      this.setState({ dishObj: newDishObj, errorPrice: '' })
    } else {
      this.setState({ errorPrice: 'Cena musi być numerem' })
    }
  }
  onChooseProducts = (e) => {
    const { dishObj } = this.state
    let newDishObj = Object.assign({}, dishObj)
    const productID = e.currentTarget.value
    console.log('PROD ', productID)
    const productToRemove = dishObj.products.find((el) => {
      if (el.id === productID) return el
    })
    console.log('PROD TO REM ', productToRemove)
    const indexOfProduct = newDishObj.products.indexOf(productToRemove)
    console.log(indexOfProduct)
    if (indexOfProduct === -1) {
      newDishObj.products.push({
        id: productID,
        quantity: 100
      })
    } else {
      newDishObj.products = [
        ...newDishObj.products.slice(0, indexOfProduct),
        ...newDishObj.products.slice(indexOfProduct + 1)
      ]
    }
    this.setState({ dishObj: newDishObj })
  }
  onChangeQuantity = (e, value) => {
    const productID = e.currentTarget.name
    const { dishObj } = this.state
    let newDishObj = Object.assign({}, dishObj)
    const productToChange = dishObj.products.find((el) => {
      if (el.id === productID) return el
    })
    console.log('PROD TO CHANGE ', productToChange)
    const indexOfProduct = newDishObj.products.indexOf(productToChange)
    console.log(indexOfProduct)
    if (indexOfProduct !== -1) {
      newDishObj.products = [
        ...newDishObj.products.slice(0, indexOfProduct),
        ...newDishObj.products.slice(indexOfProduct + 1)
      ]
      newDishObj.products.push({
        id: productID,
        quantity: parseInt(value)
      })
    }
    this.setState({ dishObj: newDishObj })
  }
  defaultProdusts = (productID) => {
    const defaultCheck = this.state.dishObj.products
    ? this.state.dishObj.products.find((prod) => {
      if (prod.id === productID) return true
    }) : null
    return defaultCheck
  }
  defaultQuantity = (productID) => {
    let defaultQuantity
    if (this.state.dishObj.products) {
      this.state.dishObj.products.find((prod) => {
        if (prod.id === productID) defaultQuantity = prod.quantity
      })
    }
    return defaultQuantity
  }
  onSubmit = () => {
    if (this.props.editedDish) {
      this.props.onEditDish(this.state.dishObj)
      this.setState({ dishObj: {} })
    } else {
      this.props.onAddDish(this.state.dishObj)
      this.setState({ dishObj: {} })
    }
  }
  sugestedPrice = () => {
    const { dishObj } = this.state
    let dishProductsPrice = 0
    dishObj.products && dishObj.products.map(product => {
      this.props.products.find(prod => {
        if (prod.id === product.id) {
          const productUsagePrice = prod.priceForKg
          ? ((prod.price * product.quantity) / 1000)
          : (prod.price * product.quantity)
          dishProductsPrice += productUsagePrice
        }
      })
    })
    return Math.round(dishProductsPrice * 2)
  }
  render () {
    this.sugestedPrice()
    console.log(this.state.dishObj)
    const actions = <div>
      <FlatButton secondary label='Anuluj' onClick={() => this.onClose()} />
      <RaisedButton primary label={this.props.editedDish ? 'Zapisz' : 'Dodaj'} onClick={() => this.onSubmit()} />
    </div>
    const products = this.props.products.map((product) => {
      return (
        <div key={product.id}>
          <Checkbox
            value={product.id}
            disabled={!this.state.dishObj.name}
            defaultChecked={!!this.defaultProdusts(product.id)}
            checkedIcon={<ActionFavorite />}
            uncheckedIcon={<ActionFavoriteBorder />}
            onCheck={(e) => this.onChooseProducts(e)}
            label={product.name}
          />
          <TextField
            name={product.id}
            onChange={this.onChangeQuantity}
            floatingLabelText={`Ilość produktu w ${product.priceForKg ? 'gramach' : 'sztukach'}`}
            defaultValue={this.defaultQuantity(product.id)}
            disabled={!this.defaultQuantity(product.id) || !this.state.dishObj.name}
            hintText={`Ilość produktu w ${product.priceForKg ? 'gramach' : 'sztukach'}`}
          />
        </div>
      )
    })
    return (
      <Dialog
        open={this.props.open}
        title={this.props.editedDish ? 'Edytuj Nowe Danie' : 'Dodaj Nowe Danie'}
        onRequestClose={this.onClose}
        autoScrollBodyContent
        actions={actions}>
        <TextField
          fullWidth
          floatingLabelText='Nazwa Dania'
          hintText='Nazwa Dania'
          defaultValue={this.state.dishObj.name ? this.state.dishObj.name : ''}
          onChange={(e) => this.onChangeName(e)} />
        <div className='flex-center' style={{ justifyContent: 'flex-start' }}>
          <TextField
            disabled={!this.state.dishObj.name}
            defaultValue={this.state.dishObj.dishPrice ? this.state.dishObj.dishPrice : ''}
            floatingLabelText='Cena Dania (w zł)'
            hintText='Cena Dania (w zł)'
            errorText={this.state.errorPrice}
            onChange={(e) => this.onChangePrice(e)} />
          <div style={{ marginLeft: '2rem', paddingTop: '2rem' }}>
            Sugerowana Cena: {this.sugestedPrice()} zł
          </div>
        </div>
        <TextField
          fullWidth
          disabled={!this.state.dishObj.name}
          defaultValue={this.state.dishObj.picUrl ? this.state.dishObj.picUrl : ''}
          floatingLabelText='URL Zdjęcia Dania'
          hintText='URL Zdjęcia Dania'
          errorText={this.state.error}
          onChange={(e) => this.onChangeURL(e)} />
        <TextField
          fullWidth
          disabled={!this.state.dishObj.name}
          multiLine
          defaultValue={this.state.dishObj.description ? this.state.dishObj.description : ''}
          rows={5}
          floatingLabelText='Opis'
          hintText='Opis'
          onChange={(e) => this.onChangeDescription(e)} />
        <Paper zDepth={1} style={{ marginTop: 15 }}>
          <div style={{ padding: 15 }}>
            <h4 style={{ marginTop: 0 }}>Wybierz Produkty Dla Tego Dania:</h4>
            {products}
          </div>
        </Paper>
      </Dialog>
    )
  }
}

AddDishModal.propTypes = {
  editedDish: PropTypes.object,
  products: PropTypes.array.isRequired,
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onEditDish: PropTypes.func.isRequired,
  onAddDish: PropTypes.func.isRequired
}

export default connect(mapStateToProps)(AddDishModal)
