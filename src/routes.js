import React from 'react'
// Import Router things
import { Router, Route, IndexRoute } from 'react-router'
import { Provider } from 'react-redux'
import store, { history } from './store'
// Import My App Components
import App from './layout/App'
import DashboardView from './views/DashboardView'
import RestaurantView from './views/RestaurantView'
import HistoryView from './views/HistoryView'
import CateringView from './views/CateringView'
import MenuView from './views/MenuView'
import ProductView from './views/ProductView'

const routes = (
  <Provider store={store}>
    <Router history={history}>
      <Route path='/' component={App}>
        <IndexRoute component={DashboardView} />
        <Route path='products' component={ProductView} />
        <Route path='restaurant/library' component={RestaurantView} />
        <Route path='restaurant/:id' component={MenuView} />
        <Route path='catering/library' component={CateringView} />
        <Route path='catering/:id' component={MenuView} />
        <Route path='history' component={HistoryView} />
      </Route>
    </Router>
  </Provider>
  )

export default routes
