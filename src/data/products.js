const products = [
  {
    id: '44jjj44j5j',
    name: 'Baklazan',
    price: 2.70,
    priceForKg: true
  },
  {
    id: '556575ggdf',
    name: 'Bataty',
    price: 3.52,
    priceForKg: true
  }
]
export default products
