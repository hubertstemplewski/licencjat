const dishes = [
  {
    id: '3456ewste5rfsasd',
    name: 'Kanapeczka',
    picUrl: 'http://img.foodnetwork.com/FOOD/2013/07/19/FNM_090113-Name-This-Dish-Stacked-Salad-Recipe_s4x3_lg.jpg',
    dishPrice: 13,
    products: [
      {
        id: '44jjj44j5j',
        quantity: 100
      },
      {
        id: '556575ggdf',
        quantity: 200
      }
    ],
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore rem, unde laboriosam illo fugit magnam sint aspernatur sapiente voluptatum, at totam aliquam sunt, itaque error. Recusandae quia voluptatibus harum nam ea temporibus laborum tenetur rerum porro ut suscipit repellendus vel, alias, adipisci cupiditate aliquid nisi, doloremque officia odit autem neque veritatis natus! Veritatis voluptatem rem enim veniam dolorem ducimus. Necessitatibus harum similique commodi nobis quaerat optio repellat quod, voluptatibus tempore cum consequuntur doloremque, accusantium odio dolor non ex. Nihil, asperiores eaque suscipit fugit natus veniam nostrum ipsam eius, laboriosam! Unde dicta magni animi, veritatis explicabo fugiat? Recusandae ullam, itaque deserunt. Libero, dolore repudiandae nihil et corporis obcaecati? Non impedit libero quisquam natus perspiciatis! At atque, in culpa! Fugiat, voluptas, odit.'
  },
  {
    id: '55988874sadasd45',
    name: 'Szpagetti',
    picUrl: 'http://www.mojegotowanie.pl/media/cache/default_view/uploads/media/default/0001/76/9a2463d45a9f7107bef30529f395814c9cd89463.jpeg',
    dishPrice: 15,
    products: [
      {
        id: '44jjj44j5j',
        quantity: 100
      },
      {
        id: '556575ggdf',
        quantity: 200
      }
    ],
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident sapiente assumenda, recusandae asperiores a ab accusantium rerum, autem accusamus natus repudiandae dicta aut, nisi! Commodi vitae voluptatibus cupiditate placeat, quasi doloremque unde dolorem vel delectus quos deleniti, aut! Ad itaque libero modi commodi earum culpa, dolore excepturi beatae ex voluptate incidunt non, soluta blanditiis fuga praesentium nam tempora ab eum dolorum corporis consequatur vero vitae enim. Perspiciatis neque maiores veritatis.'
  },
  {
    id: '3456ewsdr5rfsasd',
    name: 'Inna Kanapeczka',
    picUrl: 'http://img.foodnetwork.com/FOOD/2013/07/19/FNM_090113-Name-This-Dish-Stacked-Salad-Recipe_s4x3_lg.jpg',
    dishPrice: 15,
    products: [
      {
        id: '44jjj44j5j',
        quantity: 100
      },
      {
        id: '556575ggdf',
        quantity: 200
      }
    ],
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore rem, unde laboriosam illo fugit magnam sint aspernatur sapiente voluptatum, at totam aliquam sunt, itaque error. Recusandae quia voluptatibus harum nam ea temporibus laborum tenetur rerum porro ut suscipit repellendus vel, alias, adipisci cupiditate aliquid nisi, doloremque officia odit autem neque veritatis natus! Veritatis voluptatem rem enim veniam dolorem ducimus. Necessitatibus harum similique commodi nobis quaerat optio repellat quod, voluptatibus tempore cum consequuntur doloremque, accusantium odio dolor non ex. Nihil, asperiores eaque suscipit fugit natus veniam nostrum ipsam eius, laboriosam! Unde dicta magni animi, veritatis explicabo fugiat? Recusandae ullam, itaque deserunt. Libero, dolore repudiandae nihil et corporis obcaecati? Non impedit libero quisquam natus perspiciatis! At atque, in culpa! Fugiat, voluptas, odit.'
  },
]
export default dishes
