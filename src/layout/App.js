import React, { Component } from 'react'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import injectTapEventPlugin from 'react-tap-event-plugin'
injectTapEventPlugin()
import AppBodyView from '../views/AppBodyView'
import '../style/App.css'
import {
  MuiThemeProvider,
  lightBaseTheme
} from 'material-ui'

const muiTheme = getMuiTheme(lightBaseTheme)

class App extends Component {
  render () {
    return (
      <div className='App'>
        <MuiThemeProvider
          muiTheme={muiTheme}>
          <AppBodyView>
            {this.props.children}
          </AppBodyView>
        </MuiThemeProvider>
      </div>
    )
  }
}

export default App
