// menu actions
export function addMenu (menuObj) {
  return {
    type: 'ADD_MENU',
    menuObj
  }
}
export function editMenu (menuObj) {
  return {
    type: 'EDIT_MENU',
    menuObj
  }
}
export function removeMenu (menuObj) {
  return {
    type: 'REMOVE_MENU',
    menuObj
  }
}
// dishes actions
export function addDish (dishObj) {
  return {
    type: 'ADD_DISH',
    dishObj
  }
}
export function editDish (dishObj) {
  return {
    type: 'EDIT_DISH',
    dishObj
  }
}
export function removeDish (dishObj) {
  return {
    type: 'REMOVE_DISH',
    dishObj
  }
}
// product actions
export function addProduct (productObj) {
  return {
    type: 'ADD_PRODUCT',
    productObj
  }
}
export function editProduct (productObj) {
  return {
    type: 'EDIT_PRODUCT',
    productObj
  }
}
export function removeProduct (productObj) {
  return {
    type: 'REMOVE_PRODUCT',
    productObj
  }
}
