function dishes (state = [], action) {
  switch (action.type) {
    case 'ADD_DISH' :
      let newState = state.slice()
      newState.push(action.dishObj)
      return newState
    case 'EDIT_DISH' :
      newState = state.slice()
      newState.map((dish, i) => {
        if (dish.id === action.dishObj.id) {
          newState.splice(i, 1, action.dishObj)
        }
      })
      return newState
    case 'REMOVE_DISH':
      console.log(action.dishObj)
      const dishToRemove = state.indexOf(action.dishObj)
      return [
        ...state.slice(0, dishToRemove),
        ...state.slice(dishToRemove + 1)
      ]
    default:
      return state
  }
}

export default dishes
