function menus (state = [], action) {
  switch (action.type) {
    case 'ADD_MENU' :
      let newState = state.slice()
      newState.push(action.menuObj)
      return newState
    case 'EDIT_MENU' :
      newState = state.slice()
      newState.map((menu, i) => {
        if (menu.id === action.menuObj.id) {
          newState.splice(i, 1, action.menuObj)
        }
      })
      return newState
    case 'REMOVE_MENU':
      const menuToRemove = state.indexOf(action.menuObj)
      return [
        ...state.slice(0, menuToRemove),
        ...state.slice(menuToRemove + 1)
      ]
    default:
      return state
  }
}

export default menus
