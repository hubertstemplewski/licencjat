import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

import menus from './menus'
import dishes from './dishes'
import products from './products'

const rootReducer = combineReducers({ menus, dishes, products, routing: routerReducer })

export default rootReducer
