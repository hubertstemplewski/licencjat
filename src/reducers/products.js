function products (state = [], action) {
  switch (action.type) {
    case 'ADD_PRODUCT' :
      let newState = state.slice()
      newState.push(action.productObj)
      return newState
    case 'EDIT_PRODUCT' :
      newState = state.slice()
      newState.map((product, i) => {
        if (product.id === action.productObj.id) {
          newState.splice(i, 1, action.productObj)
        }
      })
      return newState
    case 'REMOVE_PRODUCT':
      const productToRemove = state.indexOf(action.productObj)
      return [
        ...state.slice(0, productToRemove),
        ...state.slice(productToRemove + 1)
      ]
    default:
      return state
  }
}

export default products
